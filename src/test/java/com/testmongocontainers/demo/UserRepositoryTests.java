
package com.testmongocontainers.demo;

import static org.assertj.core.api.Assertions.assertThat;

import com.testmongocontainers.demo.users.User;
import com.testmongocontainers.demo.users.UserRepository;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.testcontainers.containers.MongoDBContainer;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringBootTest
@Import(TestContainersConfig.class)
public class UserRepositoryTests {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    MongoDBContainer mongoDBContainer;

    private User user;


    @BeforeAll
    void beforeAll() {
        mongoDBContainer.start();
        this.user = UserFactory.userBuilder().build();
    }

    @Test
    @Order(1)
    void create() {
        User savedUser = userRepository.save(user);
        assertThat(savedUser).usingRecursiveComparison().isEqualTo(user);
    }

    @Test
    @Order(2)
    void find() {
        User foundUser = userRepository.findById(user.getId()).orElseThrow();
        assertThat(foundUser)
          .usingRecursiveComparison()
          .isEqualTo(user);
    }

    @Test
    @Order(3)
    void update() {
        User foundUser = userRepository.findById(user.getId()).orElseThrow();
        User updatedUser = foundUser.toBuilder().bio("update").build();
        User saved = userRepository.save(updatedUser);

        assertThat(saved)
          .usingRecursiveComparison()
          .isEqualTo(updatedUser);
    }

    @Test
    @Order(4)
    void delete() {
        userRepository.deleteById(user.getId());
        boolean exist = userRepository.existsById(user.getId());
        assertThat(exist).isFalse();
    }


}
