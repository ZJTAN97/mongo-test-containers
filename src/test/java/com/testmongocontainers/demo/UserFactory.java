package com.testmongocontainers.demo;

import com.testmongocontainers.demo.users.User;

public class UserFactory {

    public static User.UserBuilder userBuilder() {
        return User.builder()
          .id("id")
          .email("email")
          .username("username")
          .bio("bio");
    }
}
