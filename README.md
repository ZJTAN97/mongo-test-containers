# Mongo Test Containers

---

# @SpringBootTest

- The @SpringBootTest annotation is useful when we need to bootstrap the entire
  container. The annotation works by creating the ApplicationContext that will
  be utilized in our tests.
- 
- Test annotated with @SpringBootTest will bootstrap the full application
  context, which means we can @Autowire any bean that's picked up by component
  scanning into our test:

---

# @TestConfiguration

- We might want to avoid bootstrapping the real application context when
  using @SpringBootTest, but use a special test configuration. We can achieve
  this with the @TestConfiguration annotation.

- There are two ways of using the annotation. Either on a static inner class
  in the same test class where we want to @Autowire the bean OR we can
  create a separate test configuration class.
- 
- Configuration classes annotated with @TestConfiguration are excluded from
  component scanning, therefore we need to import it explicitly in every test
  where we want to @Autowire it. We can do that with the @Import annotation